package com.mobilesales.ecommerce.model

import java.io.Serializable

data  class ProductImage (

    val id: String,
    val path: String ) : Serializable
