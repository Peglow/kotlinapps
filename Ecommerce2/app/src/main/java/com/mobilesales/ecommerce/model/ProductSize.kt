package com.mobilesales.ecommerce.model

import java.io.Serializable

data class ProductSize (

    val id: String,
    val size: String) : Serializable